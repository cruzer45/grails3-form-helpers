@GrabConfig(systemClassLoader = true)
@Grapes(
		[
				@Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
		]
)


import grails.util.GrailsNameUtils

GrailsNameUtils grailsNameUtils = new GrailsNameUtils()


def listName = args[0]
def properListName = GrailsNameUtils.getClassName(listName)
def objectName = ""

listName = 
objectName = args[0]

println "\n\n"

println "${listName}List:any = [];"

println "\n\n"

println  """  download${properListName}List(){
    var link = this.url + "/api/${listName}/list";
    let headers = this.getSecureHeaders();
    let request = this.http.get(link, { headers: headers });
    request.subscribe(
      success => {
        console.info('${listName} list loaded successfully.');
        this.${listName}List = success;
        this.storage.set('${listName}List', success);
      },
      failure => {
        console.error('Failed to load ${listName} list.');
        this.storage.get('${listName}List').then(
          saved${properListName}List => {
            this.${listName}List = saved${properListName}List
          }
        );
      });
  }"""

println "\n\n"

println """this.storage.get('${listName}List').then(
        saved${properListName}List => {
        this.${listName}List = saved${properListName}List
        }
    );"""