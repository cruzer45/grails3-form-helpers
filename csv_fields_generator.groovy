@GrabConfig(systemClassLoader = true)
@Grapes(
		[
				@Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
		]
)


import grails.util.GrailsNameUtils


def shortName = args[0]
def propertyNameList = ""
def propertyList = []
if (args.size() == 2) {
	propertyNameList = args[1]
	propertyList = propertyNameList.tokenize("\n")
}

// print generic messages for this domain class
println "\n\n\n" 

if (propertyList) {

	GrailsNameUtils grailsNameUtils = new GrailsNameUtils()

	for (entry in propertyList) {
		entry = entry.trim()
		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		println " '${GrailsNameUtils.getNaturalName(entry)}',"
	}


	println "\n\n\n" 


	for (entry in propertyList) {	
		entry = entry.trim()
		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		println "${shortName}.${entry},"
	}


}

else {
	println "No properties found"
}
