def shortName = args[0]
def propertyNameList = ""
def propertyList = []
if (args.size() == 1) {
	propertyNameList = args[0]
	propertyList = propertyNameList.tokenize("\n")
}

if (propertyList) {
	for (entry in propertyList) {

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		if (entry.trim().startsWith("//")){
                    continue
                }


		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		println("${entry}")
	}



}
