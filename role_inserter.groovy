
String path = args[0]


def generateRoles(File file) {

	def fileName = file.name
	fileName = fileName.replaceAll("\\.groovy", "").replaceAll("Controller", '')

	def viewRole = "ROLE_VIEW_${fileName}"
	def createRole = "ROLE_CREATE_${fileName}"
	def editRole = "ROLE_EDIT_${fileName}"
	def deleteRole = "ROLE_DELETE_${fileName}"


	def viewAnnotation = ("@Secured('${viewRole}')")
	def createAnnotation = ("@Secured('${createRole}')")
	def editAnnotation = ("@Secured('${editRole}')")
	def deleteAnnotation = ("@Secured('${deleteRole}')")





	def lines = file.readLines()

	for (int i = 0; i < lines.size(); i++) {

		def line = lines[i]

		if (line.contains("def index")) {
			lines[i] = "${viewAnnotation}\n${line}"
		}
		else if (line.contains("def show")) {
			lines[i] = "${viewAnnotation}\n${line}"
		}
		else if (line.contains("def create")) {
			lines[i] = "${createAnnotation}\n${line}"
		}
		else if (line.contains("def save")) {
			lines[i] = "${createAnnotation}\n${line}"
		}
		else if (line.contains("def edit")) {
			lines[i] = "${editAnnotation}\n${line}"
		}
		else if (line.contains("def update")) {
			lines[i] = "${editAnnotation}\n${line}"
		}
		else if (line.contains("def delete")) {
			lines[i] = "${deleteAnnotation}\n${line}"
		}
		else if (line.contains("@Secured(['ROLE_Administrator', 'ROLE_Manager'])")) {
			lines[i] = "@Secured('IS_AUTHENTICATED_REMEMBERED')"
		}


	}

	file.text = lines.join("\n")


}


def traverseDirectory(File folder) {
	for (file in folder.listFiles()) {
		if (file.isDirectory()) {
			traverseDirectory(file)
		}

		else {
			generateRoles(file)
		}
	}
}


if (path) {
	File baseFolder = new File(path)
		if (baseFolder.isDirectory()) {
			traverseDirectory(baseFolder)
	}
	else {
			generateRoles(baseFolder)
	}


}

else {
	println("No Path specified")
}
