@GrabConfig(systemClassLoader = true)
@Grapes(
        [
                @Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
        ]
)


import grails.util.GrailsNameUtils

GrailsNameUtils grailsNameUtils = new GrailsNameUtils()



def theText = args[0]

for (entry in theText.tokenize('\n')){

        if (entry.contains("=")) {
            entry = entry.tokenize("=")[0].trim()
        }

        if (entry.trim().startsWith("//")) {
            continue
        }

        entry = entry.replaceAll("\\[[^\\[]*\\]", "")
        entry = entry.replaceAll("[A-Za-z]+ ", "").trim()


//    println (entry.tokenize(' ')[1])
    println( " \"${GrailsNameUtils.getNaturalName(entry)}\", ")

}