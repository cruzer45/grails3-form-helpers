@GrabConfig(systemClassLoader = true)
@Grapes(
		[
				@Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
		]
)
import grails.util.GrailsNameUtils

controllerAnnotations = []
roleList = []
roleEditTable = []
roleViewTable = []
String path = args[0]
beanName = args.size() > 1 ? args[0] : 'userGroup'


def generateRoles(File file) {

	def fileName = file.name
	GrailsNameUtils grailsNameUtils = new GrailsNameUtils()
	fileName = fileName.replaceAll("\\.groovy", "")
	def naturalName = grailsNameUtils.getNaturalName(fileName)

	def viewRole = "ROLE_VIEW_${fileName}"
	def createRole = "ROLE_CREATE_${fileName}"
	def editRole = "ROLE_EDIT_${fileName}"
	def deleteRole = "ROLE_DELETE_${fileName}"

	roleList.add(" '${createRole}', '${viewRole}', '${editRole}', '${deleteRole}',\n")

	controllerAnnotations.add("@Secured('${viewRole}')")
	controllerAnnotations.add("@Secured('${createRole}')")
	controllerAnnotations.add("@Secured('${editRole}')")
	controllerAnnotations.add("@Secured('${deleteRole}')")


	String roleTableEntry = """
\t<tr>
\t\t<td>${naturalName}</td>
\t\t<td class="text-center"> <g:if test="\${userGroup?.roles?.contains("${createRole}")}"> <i class="fa fa-check-square-o" aria-hidden="true"></i> </g:if> <g:else> <i class="fa fa-square-o" aria-hidden="true"></i></g:else> </td>
\t\t<td class="text-center"> <g:if test="\${userGroup?.roles?.contains("${viewRole}")}"> <i class="fa fa-check-square-o" aria-hidden="true"></i> </g:if> <g:else> <i class="fa fa-square-o" aria-hidden="true"></i></g:else> </td>
\t\t<td class="text-center"> <g:if test="\${userGroup?.roles?.contains("${editRole}")}"> <i class="fa fa-check-square-o" aria-hidden="true"></i> </g:if> <g:else> <i class="fa fa-square-o" aria-hidden="true"></i></g:else> </td>
\t\t<td class="text-center"> <g:if test="\${userGroup?.roles?.contains("${deleteRole}")}"> <i class="fa fa-check-square-o" aria-hidden="true"></i> </g:if> <g:else> <i class="fa fa-square-o" aria-hidden="true"></i></g:else> </td>
\t</tr>
"""


	roleViewTable.add(roleTableEntry)


	roleTableEntry = """
\t<tr>
\t\t<td>${naturalName}</td>
\t\t<td class="text-center">  <input type="checkbox" name="roleList" value="${createRole}" <g:if test="\${${beanName}?.roles?.contains("${createRole}")}">checked</g:if> />  </td>
\t\t<td class="text-center">  <input type="checkbox" name="roleList" value="${viewRole}"  <g:if test="\${${beanName}?.roles?.contains("${viewRole}")}">checked</g:if> />  </td>
\t\t<td class="text-center">  <input type="checkbox" name="roleList" value="${editRole}"  <g:if test="\${${beanName}?.roles?.contains("${editRole}")}">checked</g:if>  />  </td>
\t\t<td class="text-center">  <input type="checkbox" name="roleList" value="${deleteRole}"  <g:if test="\${${beanName}?.roles?.contains("${deleteRole}")}">checked</g:if>  />  </td>
\t</tr>
"""


	roleEditTable.add(roleTableEntry)


}


def traverseDirectory(File folder) {
	for (file in folder.listFiles()) {
		if (file.isDirectory()) {
			traverseDirectory(file)
		}

		else {
			generateRoles(file)
		}
	}
}


if (path) {
	File baseFolder = new File(path)

	if (baseFolder.isDirectory()) {
		traverseDirectory(baseFolder)
	}
	if (baseFolder.isFile()) {
		generateRoles(baseFolder)
	}





	println("Role Lists")
	println("================================================================================================\n\n")

	for (entry in roleList) {
		println(entry)
	}



	println("View Table Lists")
	println("================================================================================================\n\n")

	for (entry in roleViewTable) {
		print(entry)
	}



	println("Edit Table Lists")
	println("================================================================================================\n\n")

	for (entry in roleEditTable) {
		print(entry)
	}


}

else {
	println("No Path specified")
}

