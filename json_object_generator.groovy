@GrabConfig(systemClassLoader = true)
@Grapes(
		[
				@Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
		]
)


import grails.util.GrailsNameUtils

def propertyNameList = ""
def propertyList = []
def objectName = ""

propertyNameList = args[1]
objectName = args[0]

propertyList = propertyNameList.tokenize("\n")


if (objectName){
	println """
	static {
		JSON.registerObjectMarshaller(${objectName}) {
			def jsonObject = [:]
			jsonObject.id = it.id
			jsonObject.asString = it.toString()
	 """
}


if (propertyList) {

	for (entry in propertyList) {
		if (entry.contains("//") || !entry.trim()){
			continue
		}

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		println "\t\t\tjsonObject.${entry} = it.${entry}"
	}
}



if (objectName){
	println """
			return jsonObject
		}
	}
	 """
}
