@GrabConfig(systemClassLoader = true)
@Grapes(
        [
                @Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
        ]
)


import grails.util.GrailsNameUtils


def fullName = args[0]
def shortName = args[1]
def propertyNameList = ""
def propertyList = []
if (args.size() == 3) {
    propertyNameList = args[2]
    propertyList = propertyNameList.tokenize("\n")
}

// print generic messages for this domain class
println "\n\n\n"
println "# ${fullName} messages"
println "${shortName}.label=${fullName}"
println "${shortName}.add=Add ${fullName}"
println "${shortName}.create=Create ${fullName}"
println "${shortName}.edit=Edit ${fullName}"
println "${shortName}.delete=Delete ${fullName}"
println "${shortName}.list=${fullName} List"
println "${shortName}.search=${fullName} Search"
println "${shortName}.new=New ${fullName}"
println "${shortName}.show=${fullName}"
println "${shortName}.created=${fullName} {0} created"
println "${shortName}.updated=${fullName} {0} updated"
println "${shortName}.deleted=${fullName} {0} deleted"
println "${shortName}.not.found=${fullName} not found with id {0}"
println "${shortName}.not.deleted=${fullName} not deleted with id {0}"
println "${shortName}.optimistic.locking.failure=Another user has updated this ${fullName} while you were editing"
println "${shortName}.noEntries=There are no ${fullName}s recorded at this time. You may add one using the Create button above."


if (propertyList) {

    GrailsNameUtils grailsNameUtils = new GrailsNameUtils()

    for (entry in propertyList) {
        if (entry.contains("=")) {
            entry = entry.tokenize("=")[0].trim()
        }

        if (entry.trim().startsWith("//")) {
            continue
        }

        entry = entry.replaceAll("\\[[^\\[]*\\]", "")
        entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

        println "${shortName}.${entry}.label=${GrailsNameUtils.getNaturalName(entry)}"
    }


}
