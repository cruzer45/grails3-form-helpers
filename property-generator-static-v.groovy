def shortName = args[0]
def propertyNameList = ""
def propertyList = []
if (args.size() == 2) {
	propertyNameList = args[1]
	propertyList = propertyNameList.tokenize("\n")
}

if (propertyList) {
	for (entry in propertyList) {

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		String printout = """
<div class="form-group">
\t<label for="${entry}"> <g:message code="${shortName}.${entry}.label"/></label>
\t<input type="text" class="form-control" name="${entry}" id="${entry}" value="\${${shortName}.${entry}}"/>
\t<small id="${entry}-help" class="form-text text-muted">${entry} help text</small>
</div>
"""

		print(printout)
	}


	println("\n\n\n\n\n\n")

	println """
<table class="table table-striped">
	<tbody>
	"""

	for (entry in propertyList) {

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		String printout = """
		<tr>
			<td scope="row"><g:message code="${shortName}.${entry}.label"/></td>
			<td>\${${shortName}?.${entry}}</td>
		</tr>
"""
		print(printout)
	}

	println	"""
	</tbody>
</table>
	 """

}
