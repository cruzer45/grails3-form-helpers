
def propertyName = args[0]
def optionListInput = args[1]
def optionList = []
optionList = optionListInput.tokenize("\n")

for (option in optionList){
  println """<option value="${option}" <g:if test="\${${propertyName} == '${option}'}">selected</g:if>>${option}</option>"""
}
