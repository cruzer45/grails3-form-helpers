def shortName = args[0]
def propertyNameList = ""
def propertyList = []
if (args.size() == 2) {
	propertyNameList = args[1]
	propertyList = propertyNameList.tokenize("\n")
}

if (propertyList) {
	for (entry in propertyList) {

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		if (entry.trim().startsWith("//")){
                    continue
                }


		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		println("<f:field bean=\"${shortName}\" property=\"${entry}\" />")
	}


	println("\n\n\n\n\n\n")



	for (entry in propertyList) {

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		if (entry.trim().startsWith("//")){
                    continue
                }


		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		println("<f:display bean=\"${shortName}\" property=\"${entry}\"  templates=\"noLink\" />")
	}


}
