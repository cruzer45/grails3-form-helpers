@GrabConfig(systemClassLoader = true)
@Grapes(
		[
				@Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11'),
				@Grab(group = 'org.apache.commons', module = 'commons-lang3', version = '3.7')
		]
)


import grails.util.GrailsNameUtils
import org.apache.commons.lang3.text.WordUtils

def propertyNameList = args[0]
def propertyList = []
if (args.size() == 1) {

	propertyList = propertyNameList.tokenize("\n")
}

// print generic messages for this domain class


if (propertyList) {

	println("\n\n\n=====================")

	GrailsNameUtils grailsNameUtils = new GrailsNameUtils()


	for (entry in propertyList) {


		entry = entry.replaceAll("#", "number")
		entry = entry.replaceAll("Lbs.", "In Pounds")
		entry = entry.replaceAll("Lbs", "In Pounds")
		entry = entry.replaceAll("\\(", "")
		entry = entry.replaceAll("\\)", "")

		entry = WordUtils.capitalizeFully(entry.trim())

		println "BigDecimal ${GrailsNameUtils.getPropertyName(entry)}"

//		println(entry)

	}


}
