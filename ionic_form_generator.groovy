@GrabConfig(systemClassLoader = true)
@Grapes(
		[
				@Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
		]
)


import grails.util.GrailsNameUtils

def entityName =  args[0]
def propertyNameList = ""
def propertyList = []

propertyNameList = args[1]
propertyList = propertyNameList.tokenize("\n")


if (propertyList) {


println """<form #theForm="ngForm" (ngSubmit)="save()">
<ion-list>
"""

	for (entry in propertyList) {
		if (entry.contains("//") || !entry.trim()){
			continue
		}

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		println "<ion-item>\n" +
				"  <ion-label position=\"floating\">${GrailsNameUtils.getNaturalName(entry)}</ion-label>\n" +
				"  <ion-input  [(ngModel)]=\"${entityName}.${entry}\" name=\"${entry}\" required></ion-input>\n" +
				"</ion-item>\n"
	}

println """</ion-list>

<ion-button expand="block" type="submit" [disabled]="!theForm.form.valid">Save</ion-button>
    </form> """

}
