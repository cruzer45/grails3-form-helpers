
import groovy.json.StringEscapeUtils


def propertyNameList = ""
def propertyList = []
if (args.size() == 1) {
	propertyNameList = args[0]
	propertyList = propertyNameList.tokenize("|")
}

if (propertyList) {
	for (prop in propertyList) {

		String[] parts = prop.tokenize("?")
		def entry = parts[0]
		def value = parts[1]

		entry = entry.trim()

		String enumName = entry.replaceAll(" ","_").replaceAll("\\(","").replaceAll("\\)","").replaceAll("\"","").replaceAll("/","")
		enumName = enumName.replaceAll("-","").replaceAll("\\.","").replaceAll("__","_").replaceAll(",","")
		if (!Character.isLetter(enumName.charAt(0))){
			enumName = "option_"+enumName
		}

		String propertyStringValue = StringEscapeUtils.escapeJava(entry)


		String  printout = """
	${enumName}(${value}){
		@Override
		public String toString() {
			return "${propertyStringValue}"
		}
	},
	"""


		print(printout)
	}



}
