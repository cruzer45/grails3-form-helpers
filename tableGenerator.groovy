@GrabConfig(systemClassLoader = true)
@Grapes(
		[
				@Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
		]
)


import grails.util.GrailsNameUtils


def entityName = args[0]
def listName = args[1]
def variableName = args[2]
def propertyNameList = ""
def propertyList = []
if (args.size() == 4) {
	propertyNameList = args[3]
	propertyList = propertyNameList.tokenize("\n")
}

// print generic messages for this domain class
println "\n\n\n"
println "<table class=\"table table-striped table-hover table-bordered\">\n" +
		"\t<thead>\n" +
		"\t\t<tr>"

if (propertyList) {

	for (entry in propertyList) {
		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].toString().trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		println "\t\t\t<th> <g:message code=\"${entityName}.${entry}.label\"/> </th>"
	}
	println "\t\t\t<th> Action </th>"


}


println("\t\t</tr>\n" +
		"\t</thead>\n" +
		"\t<tbody>")


println("\t\t<g:each in=\"\${${listName}}\" var=\"${variableName}\">\n" +
		"\t\t\t<tr>")




if (propertyList) {

	Integer propertyCount = 0

	for (entry in propertyList) {
		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].toString().trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()



		if (propertyCount == 0){
			println "\t\t\t\t<td><g:link controller=\"\" action=\"show\" id=\"\${${variableName}?.id}\" >\${${variableName}?.${entry}}</g:link> </td>"
		}
		else{
			println "\t\t\t\t<td>\${${variableName}?.${entry}}</td>"
		}

		propertyCount ++
	}

	println "\t\t\t\t<td class=\"text-center\"> <g:link controller=\"\" action=\"show\" id=\"\${${variableName}?.id}\" class=\"btn btn-sm btn-primary\" >View</g:link> </td>"

}


println("\t\t\t</tr>\n" +
		"\t\t</g:each>\n" +
		"\t</tbody>\n" +
		"</table>")
