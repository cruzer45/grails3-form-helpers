@GrabConfig(systemClassLoader = true)
@Grapes(
		[
				@Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
		]
)


import grails.util.GrailsNameUtils



def entityName = args[0]
def propertyNameList = ""
def propertyList = []
if (args.size() == 2) {
	propertyNameList = args[1]
	propertyList = propertyNameList.tokenize("\n")

}



if (propertyList) {

	for (entry in propertyList) {
		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].toString().trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		// println "\t\t\t<th> <g:message code=\"${entityName}.${entry}.label\"/> </th>"
			String printout = """
	<tr>
		<td><g:message code="${entityName}.${entry}.label"/></td>
		<td class="text-center"> \${${entityName}?.${entry}} </td>
	</tr>
	"""

	print(printout)
	}
	// println "\t\t\t<th> Action </th>"


}


// if (propertyList) {
// 	for (entry in propertyList) {

// 		String getterName

// 		String printout = """
// 	<tr>
// 		<td><g:message code="${entityName}.${entry}.label"/></td>
// 		<td class="text-center"> \${${entityName}?.${entry}} </td>
// 	</tr>
// 	"""


// 		print(printout)
// 	}



// }
