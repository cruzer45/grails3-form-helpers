def shortName = args[0]
def propertyNameList = ""
def propertyList = []
if (args.size() == 2) {
	propertyNameList = args[1]
	propertyList = propertyNameList.tokenize("\n")
}

if (propertyList) {
	for (entry in propertyList) {

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		String printout = """

<p class="mb-0"><strong><g:message code="${shortName}.${entry}.label"/></strong></p>


<div class="form-check">
\t<input class="form-check-input" type="radio" name="${entry}" id="${entry}-positive" value="1" <g:if test="\${${entry} == true}">checked</g:if>>
\t<label for="${entry}-positive"> I want this service.</label>
</div>

<div class="form-check">
\t<input class="form-check-input" type="radio" name="${entry}" id="${entry}-negative" value="0" <g:if test="\${${entry} == false}">checked</g:if>>
\t<label for="${entry}-negative"> I DO NOT want this service.</label>
</div>

<hr/>
"""

		print(printout)
	}


println "\n\n\n\n"

for (entry in propertyList) {

	if (entry.contains("=")) {
		entry = entry.tokenize("=")[0].trim()
	}

	entry = entry.replaceAll("\\[[^\\[]*\\]", "")
	entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

	String printout = """

<div class="form-group">
		<label class="" for="${entry}"><g:message code="${shortName}.${entry}.label"/></label>
		<input type="text"  id="${entry}" class="form-control"   value="\${${shortName}.${entry} ? 'Interested' : 'Not Interested'}" readonly>
</div>

"""

	print(printout)
}


}
