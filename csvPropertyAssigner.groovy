@GrabConfig(systemClassLoader = true)
@Grapes(
        [
//                @Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
                @Grab(group = 'org.grails', module = 'grails-bootstrap', version = '4.0.1')
        ]
)
import java.lang.Object

def entityName = args[0]
def arrayName = args[1]
def propertyNameList = args[2]
def propertyList = propertyNameList.tokenize("\n")


println "\n\n\n"


if (propertyList) {

//    GrailsNameUtils grailsNameUtils = new GrailsNameUtils()

    int index = 0
    for (entry in propertyList) {

//        entry = entry.replaceAll("\\[[^\\[]*\\]", "")
//        entry = entry.replaceAll("[A-Za-z]+ ", "").trim()


        def entryParts = entry.tokenize(" ")
        def type = entryParts[0].trim()
        def variableName = entryParts[1].trim()


        if (type == "Boolean") {
            println "${entityName}.${variableName} =  Boolean.valueOf(${arrayName}[${index}].toString().trim()) "
        }
        else if (type == "BigInteger") {
            println "${entityName}.${variableName} =  new BigInteger(${arrayName}[${index}].toString().trim()) "
        }
        else if (type == "Integer") {
            println "${entityName}.${variableName} =  Integer.valueOf(${arrayName}[${index}].toString().trim()) "
        }
        else {
            println "${entityName}.${variableName} =  ${arrayName}[${index}].toString().trim()"

        }

        index++
    }


}
