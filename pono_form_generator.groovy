import grails.util.GrailsNameUtils
@GrabConfig(systemClassLoader = true)
@Grapes(
        [
                @Grab(group = 'org.grails', module = 'grails-bootstrap', version = '4.0.1'),
                @Grab(group = 'com.squareup.okhttp3', module = 'okhttp', version = '4.11.0'),
                @Grab(group = 'org.jetbrains.kotlin', module = 'kotlin-stdlib', version = '1.9.10'),
                @Grab(group = 'org.codehaus.groovy', module = 'groovy-json', version = '3.0.19')
        ]
)

import groovy.json.JsonOutput
import okhttp3.*

if (args.length == 0) {
    println("Template ID is required for this script.")
    System.exit(1)
}

def templateUUID = args[0]
String serverURL = "https://sibyl.pro"
String sibylApiKey = ""


if (!templateUUID) {
    println("Template ID is required for this script.")
    System.exit(1)
}


def login() {

    String sibylUsername = 'admin'
    String sibylPassword = 'Do01exXNoiz8EBrZJtjZmrdvpFJtAw9TZtKk'

    def requestBody = [username: sibylUsername, password: sibylPassword]

    OkHttpClient client = new OkHttpClient().newBuilder().build();
    MediaType mediaType = MediaType.parse("application/json");
    RequestBody body = RequestBody.create(mediaType, JsonOutput.toJson(requestBody));
    Request request = new Request.Builder()
            .url("https://sibyl.pro/api/login")
            .method("POST", body)
            .addHeader("Accept", "application/json")
            .addHeader("Content-Type", "application/json")
            .build();
    Response response = client.newCall(request).execute();

    if (response.code() < 300) {
        String responseString = response.body().string()
        def responseObject = new groovy.json.JsonSlurper().parseText(responseString)
        return responseObject.access_token
    }

    throw new Exception("Sibyl Login Failure")

}

def getTemplateDetails(String templateID, String serverURL) {
    String accessToken = login()

    OkHttpClient client = new OkHttpClient().newBuilder().build();
    MediaType mediaType = MediaType.parse("text/plain");
    Request request = new Request.Builder()
            .url("${serverURL}/api/integrations/openai/template/${templateID}")
            .addHeader("Authorization", "Bearer ${accessToken}")
            .build();
    Response response = client.newCall(request).execute();

    if (response.code() < 300) {
        String responseString = response.body().string()
        def responseObject = new groovy.json.JsonSlurper().parseText(responseString)
        return responseObject
    }
}

def generateHTMLForm(def templateDetails) {
    GrailsNameUtils grailsNameUtils = new GrailsNameUtils()

    String templateNameNoSpace = grailsNameUtils.getPropertyName(templateDetails.name).capitalize()

    String templateBody = """
<%@ page import="pono.auth.User" %>
<g:set var="springSecuritySvc" bean="springSecurityService"/>
<g:set var="currentUser" value="\${(pono.auth.User) springSecuritySvc.currentUser}"/>


<div class="modal-content ">
    <div class="modal-header">

        <h4 class="modal-title text-center  " id="lbl${templateNameNoSpace}" style="display: block;clear: both; width:100%; font-size: 40px;">

            <div class="text-center" style="display: block; width: 100%">
                <img src="/assets/images/companies/img-6.png" alt="" height="150">
            </div>
           ${templateDetails.name}
        </h4>

    </div>

    <div class="modal-body text-center btg-modal-body">

        <g:form controller="onboarding" action="create${templateNameNoSpace}" method="POST" class="business-plan-form">
            <g:if test="\${project}">
                <g:hiddenField name="id" value="\${project.uuid}"/>
            </g:if>

            <ul class="wizard-nav mb-5">
                <g:if test="\${!project}">

                    <li class="wizard-list-item">
                        <div class="list-item btg-list-item">
                            <div class="step-icon" data-bs-toggle="tooltip" data-bs-placement="top">
                                <i class="bx bx-user-circle"></i>
                            </div>
                        </div>
                    </li>

                </g:if>
                
                <li class="wizard-list-item">
                    <div class="list-item btg-list-item">
                        <div class="step-icon" data-bs-toggle="tooltip" data-bs-placement="top">
                            <i class="bx bx-file"></i>
                        </div>
                    </div>
                </li>
                """

    for (field in templateDetails.fields) {

        if (field.userInput) {
            templateBody = templateBody + """
                <li class="wizard-list-item">
                    <div class="list-item btg-list-item">
                        <div class="step-icon" data-bs-toggle="tooltip" data-bs-placement="top">
                            <i class="bx bx-file"></i>
                        </div>
                    </div>
                </li>
"""
        }
    }

    templateBody = templateBody + """  
            </ul>
            <!-- wizard-nav -->

            <g:if test="\${!project}">

                <div class="wizard-tab btg-wizard-tab">
                    <div class="text-center mb-4">
                        <h5>Select the project</h5>
                    </div>

                    <div>

                        <g:if test="\${project}">
                            <g:hiddenField name="id" value="\${project.uuid}"/>
                        </g:if>
                        <g:else>
                            <div class="mb-3">
                                <label for="project" class="form-label">What project will this document belong to?</label>
                                <g:select id="project" from="\${pono.project.Project.findAllByCreatedBy(currentUser)}" name="id" optionKey="uuid" optionValue="name" class="form-control"/>
                            </div>
                        </g:else>

                    </div>

                </div>
                <!-- wizard-tab btg-wizard-tab -->
            </g:if>

            <div class="wizard-tab btg-wizard-tab">
                <div>
                    <div class="text-center mb-4">
                        <h5>Great! Let's get info about the document.</h5>
                    </div>

                    <div>
                        <div class="mb-3">
                            <label for="documentName" class="form-label">What should we title this document?</label>
                            <input type="text" class="form-control" id="documentName" name="documentName" value="${templateDetails.name} - \${project?.name}" required>
                        </div>
                    </div><!-- end form -->
                </div>
            </div>
            <!-- wizard-tab btg-wizard-tab -->


"""


    for (field in templateDetails.fields) {

//        String fieldName = grailsNameUtils.getPropertyName(field.name)
        String fieldName = grailsNameUtils.getPropertyNameRepresentation(field.name.toString().toLowerCase().replace(' ', '_'))

        String fieldNameCapital = grailsNameUtils.getPropertyName(field.name).capitalize()

        if (field.userInput) {

            templateBody = templateBody +
                    """
            <div class="wizard-tab btg-wizard-tab">
                <div>
                    <div class="text-center mb-4">
                        <h5>${field.name}</h5>

                        <p>${field.helpText}</p>
                    </div>

                    <div>
                        <div class="input-group mb-3">
                            <textarea class="form-control" id="${fieldName}" name="${fieldName}" rows="3" required></textarea>
                            <button class="btn btn-outline-secondary pop-right" type="button" id="btn${fieldNameCapital}" data-toggle="popover" data-content="">
                                <i class="fa fa-lightbulb"></i>
                            </button>
                        </div>
                    </div><!-- end form -->

                </div>
            </div>
          
"""
        }
    }
    templateBody = templateBody + """

            <div class="d-flex align-items-start gap-3 mt-4">
                <button type="button" class="btn btn-primary w-sm" id="prevBtn" onclick="nextPrev(-1)">Previous</button>
                <button type="button" class="btn btn-primary w-sm ms-auto" id="nextBtn" onclick="nextPrev(1)">Next</button>
            </div>
        </g:form>

    </div>

</div>


"""


    println("\n\n Template Body is \n =================================================")
    println(templateBody)


}


def generateControllerCode(def templateDetails) {

    GrailsNameUtils grailsNameUtils = new GrailsNameUtils()

    String templateNameNoSpace = grailsNameUtils.getPropertyName(templateDetails.name).capitalize()
    String templateName  = grailsNameUtils.getPropertyName(templateDetails.name)
    String lowercaseTemplateName = templateDetails.name.toString().toLowerCase().replace(' ', '_')

    String templateBody = """
    def create${templateNameNoSpace}(String id) {
        Project project = Project.findByUuid(id)
        String documentName = params.documentName"""


    for (field in templateDetails.fields) {

        String fieldName = grailsNameUtils.getPropertyNameRepresentation(field.name.toString().toLowerCase().replace(' ', '_'))
        String fieldNameCapital = grailsNameUtils.getPropertyName(field.name).capitalize()

        if (field.userInput) {

            templateBody = templateBody + """
        String ${fieldName} = params.${fieldName}"""
        }
    }

    templateBody = templateBody + """
        Document document = sibylService.generate${templateNameNoSpace}(project, documentName, """

    for (field in templateDetails.fields) {

        String fieldName = grailsNameUtils.getPropertyNameRepresentation(field.name.toString().toLowerCase().replace(' ', '_'))
        String fieldNameCapital = grailsNameUtils.getPropertyName(field.name).capitalize()

        if (field.userInput) {
            templateBody = templateBody + "${fieldName},"
        }
    }

    //remove the trailing comma
    templateBody = templateBody[0..(templateBody.length() - 2)]


    templateBody = templateBody + """)
        flash.message = "Document generation started successfully."
        redirect(action: 'viewDocument', id: document.uuid)
    }
    
    
    
    
    def load${templateNameNoSpace}(String id) {
        Project project = Project.findByUuid(id)
        render template: '/common/newDocumentWizards/${lowercaseTemplateName}', model: [project: project]
    }
    
    
    
        else if (modalName == '${templateName}'){
            url = '\${createLink(controller: 'documentSelector', action: 'load${templateNameNoSpace}', id: project?.uuid)}'
        }
    
    
"""

    println("\n\n Controller Body is \n =================================================")
    println(templateBody)
}

def generateServiceCode(def templateDetails) {

    GrailsNameUtils grailsNameUtils = new GrailsNameUtils()
    String templateNameNoSpace = grailsNameUtils.getPropertyName(templateDetails.name).capitalize()


    String functionParameters = ""

    for (field in templateDetails.fields) {

        String fieldName = grailsNameUtils.getPropertyNameRepresentation(field.name.toString().toLowerCase().replace(' ', '_'))

        if (field.userInput) {
            functionParameters = functionParameters + " String ${fieldName},"
        }
    }

    functionParameters = functionParameters[0..(functionParameters.length() - 2)]

    String templateBody = """

    def generate${templateNameNoSpace}(Project project, String documentName,${functionParameters}) {

        String callbackLink = grailsLinkGenerator.link(controller: 'onboarding', action: 'handleCallback', absolute: true)

        User currentUser = springSecurityService.currentUser
        Document document = new Document()
        document.project = project
        document.createdBy = currentUser
        document.name = documentName ?: "${templateDetails.name} - \${project.name}"
        document.content = "-"
        document.save(flush: true, failOnError: true)

        String token = "Bearer " + login()

        def projectMetaData = JSON.parse(project.metadata)

        def requestBody = [
                name      : documentName ?: document.toString(),
                callbackUrl: callbackLink,
                template  : "${templateDetails.uuid}",
                fieldInput: ["""


    for (field in templateDetails.fields) {

        String fieldName = grailsNameUtils.getPropertyNameRepresentation(field.name.toString().toLowerCase().replace(' ', '_'))

        if (field.userInput) {
            templateBody = templateBody +
                    """
                        [
                                uuid : "${field.uuid}",
                                value: ${fieldName}
                        ],
"""
        }
    }


    templateBody = templateBody + """
                ]
        ]

        OkHttpClient client = new OkHttpClient().newBuilder().build();
        MediaType mediaType = MediaType.parse("application/json");
        RequestBody body = RequestBody.create(mediaType, (requestBody as JSON).toString());
        Request request = new Request.Builder()
                .url("https://sibyl.pro/api/integrations/openai/oneshot")
                .method("POST", body)
                .addHeader("Authorization", token)
                .addHeader("content-type", "application/json")
                .build();


        Call call = client.newCall(request);
        call.enqueue(new Callback() {
            public void onResponse(Call theCall, Response response) {
                String responseBody = response.body().string()
                def responseObject = JSON.parse(responseBody)

                println("Document creation response \\n\\n\${responseObject}")

                Document.withTransaction {
                    document.sibylID = responseObject.uuid
                    document.metaData = responseBody
                    document.save(flush: true, failOnError: true)
                }
                response.close()
            }

            public void onFailure(Call theCall, IOException e) {
            }
        }); 
        return document
    }"""

    println("\n\nService Function \n======================================")
    println(templateBody)

}


def generateMenuEntries(def templateDetails){

    GrailsNameUtils grailsNameUtils = new GrailsNameUtils()

    String templateNameNoSpace = grailsNameUtils.getPropertyName(templateDetails.name).capitalize()
    String templateName  = grailsNameUtils.getPropertyName(templateDetails.name)
    String lowercaseTemplateName = templateDetails.name.toString().toLowerCase().replace(' ', '_')

    String templateBody = """

Left Menu
=========================================

<li><a href="#" class="btnDocumentBuilder" data-document-type="${templateName}">${templateDetails.name}</a></li>



Project Page
=========================================

<a class="dropdown-item btnDocumentBuilder" href="#" data-document-type="${templateName}"><i class="mdi mdi-file-plus-outline"></i> ${templateDetails.name}</a>
   
   """


    println(templateBody)
}

def templateDetails = getTemplateDetails(templateUUID, serverURL)

//println("Template Details are \n${templateDetails}")

generateHTMLForm(templateDetails)
generateMenuEntries(templateDetails)
generateControllerCode(templateDetails)
generateServiceCode(templateDetails)



println("\n\n\n\n")
