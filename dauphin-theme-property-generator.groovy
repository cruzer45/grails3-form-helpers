def shortName = args[0]
def propertyNameList = ""
def propertyList = []
if (args.size() == 2) {
	propertyNameList = args[1]
	propertyList = propertyNameList.tokenize("\n")
}

if (propertyList) {
	for (entry in propertyList) {

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		String printout = """
<div class="form-wrapper col-sm-6">
\t<label for="${entry}">
\t\t<g:message code="${shortName}.${entry}.label"/>
\t\t<span class="required-indicator">*</span>
\t</label>
\t<div class="form-group">
\t\t<g:textField class="form-control" name="${entry}" value="\${${shortName}?.${entry}}"/>
\t</div>
</div>
"""

		print(printout)
	}


	println("\n\n\n\n\n\n")


	println("<dl class=\"dl-horizontal\">")

	for (entry in propertyList) {

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		String printout = """
\t<dt><g:message code="${shortName}.${entry}.label"/></dt>
\t<dd>\${${shortName}?.${entry}}</dd>
"""
		print(printout)
	}

	println("</dl>")

}

