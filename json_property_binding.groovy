@GrabConfig(systemClassLoader = true)
@Grapes(
		[
				@Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
		]
)


import grails.util.GrailsNameUtils

def entityName =  args[0]
def propertyNameList = ""
def propertyList = []

propertyNameList = args[1]
propertyList = propertyNameList.tokenize("\n")


if (propertyList) {
	for (entry in propertyList) {
		if (entry.contains("//") || !entry.trim()){
			continue
		}

		if (entry.contains("=")) {
			entry = entry.tokenize("=")[0].trim()
		}

		entry = entry.replaceAll("\\[[^\\[]*\\]", "")
		entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

		println " ${entityName}.${entry}  = request.JSON.${entry}"
	}


}
