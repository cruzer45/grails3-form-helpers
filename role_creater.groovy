
String path = args[0]


def generateRoles(File file) {

	def fileName = file.name
	fileName = fileName.replaceAll("\\.groovy", "").replaceAll("Controller", '')

	def viewRole = "ROLE_VIEW_${fileName}"
	def createRole = "ROLE_CREATE_${fileName}"
	def editRole = "ROLE_EDIT_${fileName}"
	def deleteRole = "ROLE_DELETE_${fileName}"


	def viewAnnotation = ("@Secured('${viewRole}')")
	def createAnnotation = ("@Secured('${createRole}')")
	def editAnnotation = ("@Secured('${editRole}')")
	def deleteAnnotation = ("@Secured('${deleteRole}')")

	println " //${fileName} Roles"
	println " '${viewRole}', '${createRole}', '${editRole}', '${deleteRole}',"
	println ""
	println "${viewAnnotation}"
	println "${createAnnotation}"
	println "${editAnnotation}"
	println "${deleteAnnotation}"

}


def traverseDirectory(File folder) {
	for (file in folder.listFiles()) {
		if (file.isDirectory()) {
			traverseDirectory(file)
		}

		else {
			generateRoles(file)
		}
	}
}


if (path) {
	File baseFolder = new File(path)
	if (baseFolder.isDirectory()) {
			traverseDirectory(baseFolder)
	}
	else {
			generateRoles(baseFolder)
	}
}

else {
	println("No Path specified")
}
