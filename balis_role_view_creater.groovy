String path = args[0]


def generateRoles(File file) {

    def fileName = file.name
    fileName = fileName.replaceAll("\\.groovy", "").replaceAll("Controller", '')
    propertyName = fileName
    propertyName = propertyName[0].toLowerCase() + propertyName.substring(1)

    def viewRole = "ROLE_VIEW_${fileName}"
    def createRole = "ROLE_CREATE_${fileName}"
    def editRole = "ROLE_EDIT_${fileName}"
    def deleteRole = "ROLE_DELETE_${fileName}"


    def viewAnnotation = ("@Secured('${viewRole}')")
    def createAnnotation = ("@Secured('${createRole}')")
    def editAnnotation = ("@Secured('${editRole}')")
    def deleteAnnotation = ("@Secured('${deleteRole}')")


    println " //${fileName} Roles"
    println " '${createRole}', '${viewRole}', '${editRole}', '${deleteRole}',"
    println ""

    // println " //${fileName} Roles"
    println """


EDIT 
=============================================

<tr>
    <td><g:message code="${propertyName}.label"/></td>
    <td class="text-center"><input type="checkbox" class="permission" data-permission="${createRole}" name="${createRole}" value="" <g:if test="\${roleString.contains('${createRole}')}">checked="checked"</g:if>/></td>
    <td class="text-center"><input type="checkbox" class="permission" data-permission="${viewRole}" name="${viewRole}" value="" <g:if test="\${roleString.contains('${viewRole}')}">checked="checked"</g:if>/></td>
    <td class="text-center"><input type="checkbox" class="permission" data-permission="${editRole}" name="${editRole}" value="" <g:if test="\${roleString.contains('${editRole}')}">checked="checked"</g:if>/></td>
    <td class="text-center"><input type="checkbox" class="permission" data-permission="${deleteRole}" name="${deleteRole}" value="" <g:if test="\${roleString.contains('${deleteRole}')}">checked="checked"</g:if>/></td>
</tr>




View 
=============================================

<tr>
    <td><g:message code="${propertyName}.label"/></td>
    <td class="text-secondary text-center"><input type="checkbox" class="permission" readonly disabled data-permission="${createRole}" name="${createRole}" value="" <g:if test="\${roleString.contains('${createRole}')}">checked="checked"</g:if>/></td>
    <td class="text-secondary text-center"><input type="checkbox" class="permission" readonly disabled data-permission="${viewRole}" name="${viewRole}" value="" <g:if test="\${roleString.contains('${viewRole}')}">checked="checked"</g:if>/></td>
    <td class="text-secondary text-center"><input type="checkbox" class="permission" readonly disabled data-permission="${editRole}" name="${editRole}" value="" <g:if test="\${roleString.contains('${editRole}')}">checked="checked"</g:if>/></td>
    <td class="text-secondary text-center"><input type="checkbox" class="permission" readonly disabled data-permission="${deleteRole}" name="${deleteRole}" value="" <g:if test="\${roleString.contains('${deleteRole}')}">checked="checked"</g:if>/></td>
</tr>
 

	"""
    // println " '${viewRole}', '${createRole}', '${editRole}', '${deleteRole}',"
    // println ""

}


def traverseDirectory(File folder) {


    for (file in folder.listFiles()) {
        if (file.isDirectory()) {
            traverseDirectory(file)
        }

        else {
            generateRoles(file)
        }
    }
}


if (path) {
    File baseFolder = new File(path)

    if (baseFolder.isDirectory()) {
        traverseDirectory(baseFolder)
    }
    else {
        generateRoles(baseFolder)
    }
}

else {
    println("No Path specified")
}
