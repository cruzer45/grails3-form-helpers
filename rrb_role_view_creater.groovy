String path = args[0]


def generateRoles(File file) {

    def fileName = file.name
    fileName = fileName.replaceAll("\\.groovy", "").replaceAll("Controller", '')
    propertyName = fileName
    propertyName = propertyName[0].toLowerCase() + propertyName.substring(1)

    def viewRole = "ROLE_VIEW_${fileName}"
    def createRole = "ROLE_CREATE_${fileName}"
    def editRole = "ROLE_EDIT_${fileName}"
    def deleteRole = "ROLE_DELETE_${fileName}"


    def viewAnnotation = ("@Secured('${viewRole}')")
    def createAnnotation = ("@Secured('${createRole}')")
    def editAnnotation = ("@Secured('${editRole}')")
    def deleteAnnotation = ("@Secured('${deleteRole}')")


    println " //${fileName} Roles"
    println " '${createRole}', '${viewRole}', '${editRole}', '${deleteRole}',"
    println ""

    // println " //${fileName} Roles"
    println """

<tr>
    <td><g:message code="${propertyName}.label"/></td>
    <td class="text-center"><input type="checkbox" name="roleList" value="${createRole}"  <g:if test="\${userGroup?.roles?.contains('${createRole}')}">checked</g:if>/></td>
    <td class="text-center"><input type="checkbox" name="roleList" value="${viewRole}"    <g:if test="\${userGroup?.roles?.contains('${viewRole}')}">checked</g:if>/></td>
    <td class="text-center"><input type="checkbox" name="roleList" value="${editRole}"    <g:if test="\${userGroup?.roles?.contains('${editRole}')}">checked</g:if>/></td>
    <td class="text-center"><input type="checkbox" name="roleList" value="${deleteRole}"  <g:if test="\${userGroup?.roles?.contains('${deleteRole}')}">checked</g:if>/></td>
</tr>


<tr>
     <td><g:message code="${propertyName}.label"/></td>
     <td class="text-center"><g:if test="\${userGroup?.roles?.contains("${createRole}")}"><i class="fa fa-check-square-o" aria-hidden="true"></i></g:if> <g:else><i class="fa fa-square-o" aria-hidden="true"></i></g:else></td>
     <td class="text-center"><g:if test="\${userGroup?.roles?.contains("${viewRole}")}"><i class="fa fa-check-square-o" aria-hidden="true"></i></g:if> <g:else><i class="fa fa-square-o" aria-hidden="true"></i></g:else></td>
     <td class="text-center"><g:if test="\${userGroup?.roles?.contains("${editRole}")}"><i class="fa fa-check-square-o" aria-hidden="true"></i></g:if> <g:else><i class="fa fa-square-o" aria-hidden="true"></i></g:else></td>
     <td class="text-center"><g:if test="\${userGroup?.roles?.contains("${deleteRole}")}"><i class="fa fa-check-square-o" aria-hidden="true"></i></g:if> <g:else><i class="fa fa-square-o" aria-hidden="true"></i></g:else></td>
 </tr>



	"""
    // println " '${viewRole}', '${createRole}', '${editRole}', '${deleteRole}',"
    // println ""

}


def traverseDirectory(File folder) {


    for (file in folder.listFiles()) {
        if (file.isDirectory()) {
            traverseDirectory(file)
        }

        else {
            generateRoles(file)
        }
    }
}


if (path) {
    File baseFolder = new File(path)

    if (baseFolder.isDirectory()) {
        traverseDirectory(baseFolder)
    }
    else {
        generateRoles(baseFolder)
    }
}

else {
    println("No Path specified")
}
