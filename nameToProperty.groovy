@GrabConfig(systemClassLoader = true)
@Grapes(
        [
//                @Grab(group = 'org.grails', module = 'grails-bootstrap', version = '3.2.11')
                @Grab(group = 'org.grails', module = 'grails-bootstrap', version = '4.0.1')
        ]
)


import grails.util.GrailsNameUtils


def propertyNameList = args[0]
def propertyList = propertyNameList.tokenize("\n")


println "\n\n\n"


if (propertyList) {

    GrailsNameUtils grailsNameUtils = new GrailsNameUtils()

    for (entry in propertyList) {
//        if (entry.contains("=")) {
//            entry = entry.tokenize("=")[0].trim()
//        }
//
//        if (entry.trim().startsWith("//")) {
//            continue
//        }

//        entry = entry.replaceAll("\\[[^\\[]*\\]", "")
//        entry = entry.replaceAll("[A-Za-z]+ ", "").trim()

        println "String ${GrailsNameUtils.getPropertyName(entry)}"
    }


}
